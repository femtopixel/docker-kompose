> [!CAUTION]
> As-of 2021, this product does not have a free support team anymore. If you want this product to be maintained, please support my work.
 
> [!NOTE]
> (This product is available under a free and permissive license, but needs financial support to sustain its continued improvements. In addition to maintenance and stability there are many  desirable features yet to be added.)
 
> [!TIP]
> THIS REPOSITORY IS AUTO-UPDATED BY [GITHUB-RELEASE-NOTIFIER](https://github.com/femtopixel/github-release-notifier) (https://github.com/femtopixel/github-release-notifier)

![logo](logo.png)

Kompose - Docker Image
======================

[![Docker Pulls](https://img.shields.io/docker/pulls/femtopixel/kompose.svg)](https://hub.docker.com/r/femtopixel/kompose/)
[![Docker Stars](https://img.shields.io/docker/stars/femtopixel/kompose.svg)](https://hub.docker.com/r/femtopixel/kompose/)
[![PayPal donation](https://github.com/jaymoulin/jaymoulin.github.io/raw/master/ppl.png "PayPal donation")](https://www.paypal.me/jaymoulin)
[![Buy me a coffee](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png "Buy me a coffee")](https://www.buymeacoffee.com/jaymoulin)
[![Buy me a coffee](https://ko-fi.com/img/githubbutton_sm.svg "Buy me a coffee")](https://www.ko-fi.com/jaymoulin)

Description
-----------

The purpose of this image is to use [Kubernetes Kompose](https://github.com/kubernetes/kompose) tool to convert Docker-compose syntax easily thanks to Docker technology 

 Usage
------

```
docker run --rm --name kompose -it -v /path/to/your/folder:/src femtopixel/kompose  
```

Please check [Kubernetes Kompose](https://github.com/kubernetes/kompose) documentation for further information
