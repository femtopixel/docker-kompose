FROM golang:alpine as builder
COPY qemu-*-static /usr/bin/
ARG VERSION=v1.34.0
ARG ARM=0
WORKDIR $GOPATH/src/github.com/kubernetes/kompose
RUN apk add git gcc g++ --update --no-cache && \
git clone https://github.com/kubernetes/kompose . && \
git checkout ${VERSION} && \
go get && \
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o /usr/bin/kompose main.go

FROM alpine
ARG VERSION=v1.34.0
LABEL maintainer="Jay MOULIN <jaymoulin@gmail.com>"
LABEL version=${VERSION}-${TARGETPLATFORM}
COPY --from=builder /usr/bin/kompose /usr/bin/
WORKDIR /src
VOLUME /src
ENTRYPOINT ["kompose"]
